﻿using HiitsPdfPrint.Models;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HiitsPdfPrint.Controllers
{
    public class HomeController : Controller
    {
        static List<Student> studentsCollection = new List<Student>();

        

        private void GeneratePDF(Document doc)
        {
            //fonts
            var normalFont = FontFactory.GetFont(FontFactory.HELVETICA, 12);
            var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);
            var largeBoldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 48);
          
            foreach (Student student in studentsCollection)
            {
                doc.NewPage();
                oneColum(200f, doc, student.Name, largeBoldFont);
                foreach (string module in student.Modules)
                {
                    twoColumns(10f, doc, module, DateTime.Today.ToString("dd/MM/yyyy"), boldFont);
                }
                fourColumns(190f, doc, string.Empty, string.Empty, DateTime.Today.ToString("dd/MM/yyyy"), "12345678", normalFont);
            }


        }

        private void twoColumns(float leading, Document doc, string leftString, string rightString, dynamic font)
        {
            Paragraph main = new Paragraph(leading);
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new VerticalPositionMark());
            ph1.Add(new Chunk(Environment.NewLine));
            ph1.Add(new Chunk(leftString)); // Here I add the string on the left as a chunk into Phrase.    
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(rightString)); // Here I add the string on the right as a chunk into same phrase.    
            main.Add(ph1);
            doc.Add(main);
        }

        private void threeColumns(float leading, Document doc, string leftString, string centerString, string rightString, dynamic font)
        {
            Paragraph main = new Paragraph(leading);
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new VerticalPositionMark());
            ph1.Add(new Chunk(Environment.NewLine));
            ph1.Add(new Chunk(leftString)); // Here I add the string on the right as a chunk into Phrase.      
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(centerString)); // Here I add the string in the middle as a chunk into Phrase. 
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(rightString)); // Here I add the string on the right as a chunk into Phrase. 
            main.Add(ph1);
            doc.Add(main);
        }

        private void fourColumns(float leading, Document doc, string leftString, string centerString, string rightString, string outerString, dynamic font)
        {
            Paragraph main = new Paragraph(leading);
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new VerticalPositionMark());
            ph1.Add(new Chunk(Environment.NewLine));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(glue); // Here I add special chunk to the same phrase.
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(glue); // Here I add special chunk to the same phrase.   
            ph1.Add(glue); // Here I add special chunk to the same phrase.
            ph1.Add(glue); // Here I add special chunk to the same phrase. 
            ph1.Add(new Chunk(rightString)); // Here I add the string on the right as a chunk into Phrase. 
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(outerString)); // Here I add the string on the right as a chunk into Phrase. 
            main.Add(ph1);
            doc.Add(main);
        }

        private void oneColum(float leading, Document doc, string text, dynamic font)
        {
            doc.Add(new Paragraph(leading, text, font));
            doc.Add(new Paragraph(100f, Environment.NewLine));
        }

        private void renderPdf(byte[] bytes)
        {
            Response.Clear();
            if (bytes != null)
            {
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-length", bytes.Length.ToString());
                Response.BinaryWrite(bytes);
            }
        }

        public void PrintCert()
        {
            loadStudents();
            byte[] bytes;
            using (MemoryStream ms = new MemoryStream())
            {
                using (Document doc = new Document(PageSize.A4, 60f, 60f, 60f, 2f))
                {
                    using (PdfWriter w = PdfWriter.GetInstance(doc, ms))
                    {
                        doc.Open();
                        GeneratePDF(doc);
                        doc.Close();
                        bytes = ms.ToArray();
                    }
                }
            }

            renderPdf(bytes);
            
        }

        public ActionResult Index()
        {
            return View();
        }

       

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        public static void loadStudents()
        {
            Student student;
            student = new Student("Khaya Khumalo", new DateTime(2010, 8, 18) , new string[] {"2dcad","word-processing","maths" });
            studentsCollection.Add(student);
            student = new Student("Alex Fanning", new DateTime(2012, 9, 3), new string[] { "spreadsheets", "word-processing", "english" });
            studentsCollection.Add(student);
        }

    }
}