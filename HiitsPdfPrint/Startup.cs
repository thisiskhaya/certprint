﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HiitsPdfPrint.Startup))]
namespace HiitsPdfPrint
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
